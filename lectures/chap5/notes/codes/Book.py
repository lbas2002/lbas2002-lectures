class Book:
    def __init__(self, title=None, edition=None, price=None, currency=None):
        # instance attributes
        self.title = title
        self.authors = []
        self.chapters = []
        self.edition = edition
        self.price = price
        self.currency = currency

    # Getters and setters for instance attributes
    def get_title(self):
        return self.title

    def set_title(self, title):
        self.title = title

    def get_pages(self):
        return self.pages

    def set_pages(self, pages):
        self.pages = pages

    def add_author(self,author):
        self.authors.append(author)

    def add_chapter(self,chapter):
        self.chapters.append(chapter)

    def get_edition(self):
        return self.edition

    def set_edition(self, edition):
        self.edition = edition

    def get_price(self):
        return self.price

    def set_price(self, price):
        self.price = price

    def get_currency(self):
        return self.currency

    def set_currency(self, currency):
        self.currency = currency

    # Magic methods
    def __str__(self):
        return f'{self.title}, {self.authors}, {self.edition}, {self.price:0.2f} {self.currency}, {self.chapters}'